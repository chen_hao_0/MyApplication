package jrtt.imust.com.myapplication.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by DELL on 2020/7/3.
 */

public class MyDBHelper extends SQLiteOpenHelper {
    public static final String TAG="MyDBHelper";
    public MyDBHelper(Context context){
        super(context,"finaldb",null,1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG,"onCreate"+"创建数据库");
        db.execSQL("create table cdinfo(id varchar(20),type varchar(20))");
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
       Log.i(TAG,"onUpgrade:"+"数据库升级");
    }
}
