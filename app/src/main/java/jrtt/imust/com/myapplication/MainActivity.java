package jrtt.imust.com.myapplication;

import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import jrtt.imust.com.myapplication.utils.MyDBHelper;

public class MainActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor see;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyDBHelper helper = new MyDBHelper(getApplicationContext());
        db = helper.getReadableDatabase();
        Button btnAdd = findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                db.execSQL("insert into cdinfo (id,type) values (?,?)", new Object[]{"1001", "make"});
            }
        });
        ListView lvList=findViewById(R.id.lv_list);
        see=db.rawQuery("select * from cdinfo",null);
        lvList.setAdapter(new MyAdpter());
    }
    class MyAdpter extends BaseAdapter {

        @Override
        public int getCount() {
            return see.getCount();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            Log.i("TAG",i+"");
            see.moveToPosition(i);
            String id=see.getString(0);
            String type=see.getString(1);
            TextView tvName=new TextView(getApplicationContext());
            String a="id="+"，type="+type;
            tvName.setText(a);
            return tvName;
        }
    }
}
